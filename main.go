package main

import (
	"gitlab.com/packatrust/storj/cmd"
)


func main() {
	cmd.Execute()
}