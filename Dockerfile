FROM golang:1.15.2-alpine3.12


RUN apk add git

WORKDIR /app
COPY . .
RUN go get storj.io/uplink
RUN go get github.com/spf13/cobra
RUN go get gitlab.com/packatrust/storj
RUN go build

# RUN ls uplink
# RUN cp uplink /usr/local/bin/uplink

LABEL io.packatrust="go-storj"
LABEL version="0.1.0"
LABEL description="PKT storj provider"
