package utils

import (
	"bytes"
	"fmt"
)

// Check ...
func Check(downloaded, uploaded []byte) error {
	// Check that the downloaded data is the same as the uploaded data.
	if !bytes.Equal(downloaded, uploaded) {
		return fmt.Errorf("got different object back: %q != %q", uploaded, downloaded)
	}
	return nil
}