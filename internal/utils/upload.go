// See LICENSE for copying information.

package utils

import (
	"bytes"
	"fmt"
	"io"
	"context"
)

// Upload ...
func Upload( 
	satellite, 
	apiKey, 
	passphrase, 
	bucketName, 
	uploadKey, 
	data string) error {

	ctx := context.Background()
	project := access(ctx, satellite, apiKey, passphrase)

	// Ensure the desired Bucket within the Project is created.
	_, err := project.EnsureBucket(ctx, bucketName)
	if err != nil {
		return fmt.Errorf("could not ensure bucket: %v", err)
	}

	// Intitiate the upload of our Object to the specified bucket and key.
	upload, err := project.UploadObject(ctx, bucketName, uploadKey, nil)
	if err != nil {
		return fmt.Errorf("could not initiate upload: %v", err)
	}

	// Copy the data to the upload.
	buf := bytes.NewBuffer([]byte(data))
	_, err = io.Copy(upload, buf)
	if err != nil {
		_ = upload.Abort()
		return fmt.Errorf("could not upload data: %v", err)
	}

	// Commit the uploaded object.
	err = upload.Commit()
	if err != nil {
		return fmt.Errorf("could not commit uploaded object: %v", err)
	}

	return nil
}