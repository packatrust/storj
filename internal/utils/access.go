package utils

import (
	"storj.io/uplink"
	"context"
)



func access (ctx context.Context, satellite, apiKey, passphrase string) *uplink.Project  {
		// Request access grant to the satellite with the API key and passphrase.
		access, _ := uplink.RequestAccessWithPassphrase(ctx, satellite, apiKey, passphrase)
	
		// Open up the Project we will be working with.
		project, _ := uplink.OpenProject(ctx, access)
		defer project.Close()
		
		return project
}