package utils

import (
	"context"
	"io/ioutil"
)

// Download ...
func Download(
	satellite, 
	apiKey, 
	passphrase, 
	bucketName, 
	objectKey string) []byte {

	ctx := context.Background()
	project := access(ctx, satellite, apiKey, passphrase)

	// Initiate a download of the same object again
	download, _ := project.DownloadObject(ctx, bucketName, objectKey, nil)
	defer download.Close()

	// Read everything from the download stream
	receivedContents, _ := ioutil.ReadAll(download)

	return receivedContents
}
