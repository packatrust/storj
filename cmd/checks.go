package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/packatrust/storj/internal/utils"
)

var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "Check a downloaded package against a given binary",
	Long: `Check a downloaded package against a given binary`,

	Run: func(cmd *cobra.Command, args []string) {
		downloaded := utils.Download(satellite, apikey, passphrase, bucket, data)
		utils.Check(downloaded, []byte(data))
	},
}