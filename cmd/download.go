package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/packatrust/storj/internal/utils"
)

var downloadCmd = &cobra.Command{
	Use:   "download",
	Short: "Download a package",
	Long: `Download package after it has been sign through the smart contract`,

	Run: func(cmd *cobra.Command, args []string) {
		utils.Download(satellite, apikey, passphrase, bucket, data)
	},
}