package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/packatrust/storj/internal/utils"
)

var uploadCmd = &cobra.Command{
	Use:   "upload",
	Short: "Upload a package",
	Long: `Upload package after it has been sign through the smart contract`,

	Run: func(cmd *cobra.Command, args []string) {

		utils.Upload(satellite, apikey, passphrase, bucket, dir, data)
	},
}
