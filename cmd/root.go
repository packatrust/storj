package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var (

	bucket string
	dir string
	data string

	satellite = os.Getenv("SATELLITE")
	passphrase = os.Getenv("API_KEY")
	apikey = os.Getenv("PASSPHRASE")

	rootCmd = &cobra.Command{
		Use:   "storj",
		Short: "PackaTrust-Storj adapter",
		Long: `storj is a Storj wrapper from PackaTrust to ensure the interoperability of software 
	storage during the build, release, and installation by checking the authenticity in a blockchain`,
	
		// App main action
		Run: func(cmd *cobra.Command, args []string) {
			/*rpcEndpoint, _ := cmd.Flags().GetString("rpc")
	
			if rpcEndpoint == "" {
				log.Fatal("Please provide --rpcEndpoint option")
			}*/
		},
	}
)

func init() {
	rootCmd.PersistentFlags().StringVarP(&bucket, "bucket", "b", "", "bucket name (required)")
	rootCmd.MarkFlagRequired("bucket")

	rootCmd.PersistentFlags().StringVarP(&dir, "dir", "D", "", "dir name (required)")
	rootCmd.MarkFlagRequired("dir")

	rootCmd.PersistentFlags().StringVarP(&data, "data", "d", "", "data name (required)")
	rootCmd.MarkFlagRequired("data")

	rootCmd.AddCommand(uploadCmd)
	rootCmd.AddCommand(downloadCmd)
	rootCmd.AddCommand(checkCmd)
}

// Execute ...
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}