module gitlab.com/packatrust/storj

go 1.15

require (
	github.com/spf13/cobra v1.0.0
	storj.io/uplink v1.3.0
)
